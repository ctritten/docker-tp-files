#!/bin/bash

if [ -f /run/secrets/mysql-password ] ; then
    tail -n 1 /run/secrets/mysql-password
    export DB_PASSWORD="$(tail -n 1 /run/secrets/mysql-password)"
fi

rm -f /run/apache2/apache2.pid
echo "PassEnv DB_USER DB_PASSWORD DB_NAME DB_HOST" > /etc/apache2/conf-available/env.conf
a2enconf env
exec /usr/sbin/apache2ctl -D FOREGROUND
